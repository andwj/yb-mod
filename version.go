// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "fmt"
import "strings"

// Version is a semantic version ("semver") value.
// see https://semver.org/ for more information.
//
// NOTE: this implementation has limited support for the
//       pre-release components, and NO support for build
//       metadata (except to ignore it).
type Version struct {
	str   string
	valid bool

	major int
	minor int
	patch int

	pre1 string
	pre2 string
	pre3 string
	pre4 string
}

// Encode recreates the 'str' field from the current componets.
// The 'valid' field is checked and is not modified, and when
// false the generated string is always "".
func (v *Version) Encode() {
	if v.valid {
		v.str = fmt.Sprintf("%d.%d.%d", v.major, v.minor, v.patch)

		if v.pre1 != "" {
			v.str = fmt.Sprintf("%s-%s", v.str, v.pre1)
		}
		if v.pre2 != "" {
			v.str = fmt.Sprintf("%s.%s", v.str, v.pre2)
		}
		if v.pre3 != "" {
			v.str = fmt.Sprintf("%s.%s", v.str, v.pre3)
		}
		if v.pre4 != "" {
			v.str = fmt.Sprintf("%s.%s", v.str, v.pre4)
		}
	} else {
		v.str = ""
	}
}

// Decode parses the 'str' field into the components of a
// semver version (major, minor, patch, etc).  On success,
// the 'valid' field is set to true and Ok is returned.
// On failure, the 'valid' field is set to false and a
// error message is returned.  If the decode fails, then
// all the component fields must be considered as junk.
func (v *Version) Decode() error {
	v.valid = false

	s := v.str

	if s == "" {
		return fmt.Errorf("version string is empty")
	}

	// strip a 'v' prefix -- strictly it is not semver
	// compatible, but common enough to handle it here.
	if len(s) > 0 && s[0] == 'v' {
		s = s[1:]
	}

	// strip any build metadata
	i := strings.IndexRune(s, '+')
	if i >= 0 {
		s = s[0:i]
	}

	// split off the pre-release part (if any)
	prerel := ""

	i = strings.IndexRune(s, '-')
	if i >= 0 {
		prerel = s[i+1:]
		s = s[0:i]
	}

	// handle the major/minor/patch.
	//
	// NOTE: we allow just the major, or major + minor, which
	//       is not strictly semver compatible.
	//       [ TODO justify this or make it optional ]
	v.major = 0
	v.minor = 0
	v.patch = 0

	if n, _ := fmt.Sscanf(s, "%d.%d.%d", &v.major, &v.minor, &v.patch); n == 3 {
		// ok
	} else if n, _ := fmt.Sscanf(s, "%d.%d", &v.major, &v.minor); n == 2 {
		// ok
	} else if n, _ := fmt.Sscanf(s, "%d", &v.major); n == 1 {
		// ok
	} else {
		return fmt.Errorf("invalid version string")
	}

	// handle the pre-release part
	v.pre1 = ""
	v.pre2 = ""
	v.pre3 = ""
	v.pre4 = ""

	if prerel != "" {
		parts := strings.Split(prerel, ".")
		for len(parts) > 0 && parts[0] == "" {
			parts = parts[1:]
		}
		if len(parts) > 0 {
			v.pre1 = parts[0]
		}
		if len(parts) > 1 {
			v.pre2 = parts[1]
		}
		if len(parts) > 2 {
			v.pre3 = parts[2]
		}
		if len(parts) > 3 {
			v.pre4 = parts[3]
		}
	}

	v.valid = true
	return Ok
}

// Compare performs a comparison of version 'v1' with 'v2'.
// It returns -1 if v1 < v2, +1 if v1 > v2, 0 if equal.
// It looks at the components and ignores the 'str' field.
// This should not be called if either Version is invalid.
func (v1 *Version) Compare(v2 *Version) int {
	switch {
	case v1.major < v2.major:
		return -1
	case v1.major > v2.major:
		return +1

	case v1.minor < v2.minor:
		return -1
	case v1.minor > v2.minor:
		return +1

	case v1.patch < v2.patch:
		return -1
	case v1.patch > v2.patch:
		return +1

	// a pre-release version is always < a normal version
	case v1.pre1 != "" && v2.pre1 == "":
		return -1
	case v1.pre1 == "" && v2.pre1 != "":
		return +1

	case PR_Less(v1.pre1, v2.pre1):
		return -1
	case PR_Less(v2.pre1, v1.pre1):
		return +1

	case PR_Less(v1.pre2, v2.pre2):
		return -1
	case PR_Less(v2.pre2, v1.pre2):
		return +1

	case PR_Less(v1.pre3, v2.pre3):
		return -1
	case PR_Less(v2.pre3, v1.pre3):
		return +1

	case PR_Less(v1.pre4, v2.pre4):
		return -1
	case PR_Less(v2.pre4, v1.pre4):
		return +1
	}

	return 0
}

// LessThan tests whether 'v1' is less than 'v2'.
// It looks at the components and ignores the 'str' field.
// This should not be called if either Version is invalid.
func (v1 *Version) LessThan(v2 *Version) bool {
	return v1.Compare(v2) < 0
}

// GreaterThan tests whether 'v1' is greater than 'v2'.
// It looks at the components and ignores the 'str' field.
// This should not be called if either Version is invalid.
func (v1 *Version) GreaterThan(v2 *Version) bool {
	return v1.Compare(v2) > 0
}

// Equal tests whether 'v1' is exactly the same as 'v2'.
// It looks at the components and ignores the 'str' field.
// This should not be called if either Version is invalid.
func (v1 *Version) Equal(v2 *Version) bool {
	return v1.Compare(v2) == 0
}

// PR_Less tests whether the pre-release component 'p1'
// is less than 'p2'.  It implements the semver rules for
// pre-release components, namely that strings consisting of
// only digits are compared numerically, and such strings
// are always lower than any non-numeric string.
func PR_Less(p1, p2 string) bool {
	switch {
	case p1 == "" && p2 != "":
		return true

	case p2 == "" && p1 != "":
		return false
	}

	var err1, err2 error
	var num1, num2 int

	_, err1 = fmt.Sscanf(p1, "%d", &num1)
	_, err2 = fmt.Sscanf(p2, "%d", &num2)

	switch {
	case err1 == nil && err2 == nil:
		return num1 < num2

	case err1 == nil:
		return true

	case err2 == nil:
		return false
	}

	return p1 < p2
}
