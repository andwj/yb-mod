
YB-MOD
======

by Andrew Apted, 2019.


About
-----

Yewberry is a programming language currently in development,
and **yb-mod** will be a command-line tool which assists in
setting up module files, downloading dependencies from the
internet, and upgrading and downgrading module versions.

The following document describes the module system:
[MODULES.md](https://gitlab.com/andwj/yewberry/blob/master/MODULES.md)


Status
------

This project is in early stages of development.


Legalese
--------

yb-mod is Copyright &copy; 2019 Andrew Apted.

yb-mod is Free Software, under the terms of the GNU General
Public License, version 3 or (at your option) any later version.
See the [LICENSE.txt](LICENSE.txt) file for the complete text.

yb-mod comes with NO WARRANTY of any kind, express or implied.
Please read the license for full details.

