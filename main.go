// Copyright 2019 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "os"
import "fmt"

import "gitlab.com/andwj/argv"

var Ok error

var Options struct {
	help    bool
	version bool
}

func main() {
	// TODO
	argv.Enabler("h", "help", &Options.help, "display this help text")

	err := argv.Parse()
	if err != nil {
		fmt.Fprintf(os.Stderr, "yb-mod: %s\n", err.Error())
		os.Exit(1)
	}

	// TODO

	os.Exit(0)
}
