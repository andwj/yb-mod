// Copyright 2018 Andrew Apted.
// This code is under the GNU General Public License, version 3
// or (at your option) any later version.

package main

import "io"
import "fmt"
import "bufio"
import "strings"
import "unicode"
import "unicode/utf8"

type TokenKind int

const (
	TOK_EOF TokenKind = iota
	TOK_ERROR

	// simple tokens
	TOK_Int
	TOK_Float
	TOK_Char
	TOK_String

	TOK_Name
	TOK_Module

	// compound tokens
	TOK_Expr
	TOK_Array
	TOK_Tuple
	TOK_Map
	TOK_Set
)

type Token struct {
	Kind     TokenKind
	Children []*Token
	Str      string
	Module   string // for TOK_Name: preceding module name (if any)
	LineNum  int
	Info     *NodeInfo
}

type NodeInfo struct {
}

const ID_CHARS = "@_.-+%!#$&*/<=>?'^|~"

type Lexer struct {
	reader   *bufio.Reader
	line_num int
	finished bool
	tokens   []*Token
	pending  *Token
}

// NewLexer creates a Lexer from a Reader (e.g. a file).
func NewLexer(r io.Reader) *Lexer {
	lexer := &Lexer{}
	lexer.reader = bufio.NewReader(r)
	lexer.tokens = make([]*Token, 0)
	return lexer
}

// Scan scans the file and returns the next high-level token.
// If the file reaches EOF, or has reached it before, then a TOK_EOF
// token is returned.  If a token has been pushed back, then that
// token is returned (and the push-back slot is cleared).
//
// Any problems scanning the file will return a TOK_ERROR token,
// which covers both I/O errors and malformed text errors.
func (lex *Lexer) Scan() *Token {
	if lex.pending != nil {
		t := lex.pending
		lex.pending = nil
		return t
	}

	t := lex.rawNext()

	// handle a scoped identifier (module name + normal name)
	/* NOTE: DISABLED FOR yb-mod
	if t.Kind == TOK_Module {
		next := lex.rawNext()

		switch next.Kind {
		case TOK_EOF:
			return t
		case TOK_ERROR:
			return next
		case TOK_Name:
			next.Module = t.Str
			return next
		case TOK_Module:
			msg := "too many module scopes"
			return &Token{Kind: TOK_ERROR, Str: msg, LineNum: t.LineNum}
		default:
			lex.PushBack(next)
			return t
		}
	} */

	if t.Match("(") {
		return lex.scanElements(TOK_Expr, "(", ")")
	}
	if t.Match("[") {
		return lex.scanElements(TOK_Array, "[", "]")
	}
	if t.Match("{") {
		return lex.scanElements(TOK_Tuple, "{", "}")
	}
	if t.Match("{|") {
		return lex.scanElements(TOK_Map, "{|", "|}")
	}
	if t.Match("(|") {
		return lex.scanElements(TOK_Set, "(|", "|)")
	}

	return t
}

// PushBack pushes back a token, which will returned on a subsequent
// call to Scan().  Only a single push-back is supported.
func (lex *Lexer) PushBack(t *Token) {
	if lex.pending != nil {
		panic("lexer: tried to push back twice")
	}

	lex.pending = t
}

func (lex *Lexer) scanElements(kind TokenKind, opener, closer string) *Token {
	expr := Token{Kind: kind, LineNum: lex.line_num}
	expr.Children = make([]*Token, 0)

	for {
		t := lex.Scan()

		if t.Kind == TOK_ERROR {
			return t
		}

		// found the closing bracket?
		if t.Match(closer) {
			return &expr
		}

		// the REPL requires treating EOF with a different error.
		// otherwise (def a ]) is never "completeable".

		if t.Kind == TOK_EOF {
			msg := "unterminated expr in " + opener + closer

			// for this error, use line number of opening bracket
			return &Token{Kind: TOK_ERROR, Str: msg, LineNum: expr.LineNum}
		}

		if t.Match(")") || t.Match("]") || t.Match("}") ||
			t.Match("|}") || t.Match("|)") {

			msg := "badly terminated expr in " + opener + closer

			// for this error, use line number of opening bracket
			return &Token{Kind: TOK_ERROR, Str: msg, LineNum: expr.LineNum}
		}

		expr.Add(t)
	}
}

// rawNext scans the file and returns the next low-level token.
// If the file reaches EOF, or has reached it before, then a TOK_EOF
// token is returned.
//
// Any problems scanning the file will return a TOK_ERROR token,
// which covers both I/O errors and malformed text errors.
func (lex *Lexer) rawNext() *Token {
	for {
		if len(lex.tokens) > 0 {
			t := lex.tokens[0]
			lex.tokens = lex.tokens[1:]
			return t
		}

		// once finished, keep returning EOF
		// [ this simplifies some logic in the calling code ]
		if lex.finished {
			return &Token{Kind: TOK_EOF, LineNum: lex.line_num}
		}

		// need to scan the next line
		lex.line_num += 1

		// NOTE: this can return some data + io.EOF
		line, err := lex.reader.ReadString('\n')

		if err == io.EOF {
			lex.finished = true
		} else if err != nil {
			lex.finished = true
			return &Token{Kind: TOK_ERROR, Str: err.Error(), LineNum: lex.line_num}
		}

		lex.scanLine(line)
	}
}

//----------------------------------------------------------------------

func (t *Token) Add(child *Token) {
	t.Children = append(t.Children, child)
}

func (t *Token) Prepend(child *Token) {
	t.Children = append(t.Children, nil)
	copy(t.Children[1:], t.Children[0:])
	t.Children[0] = child
}

// Match is a convenience method for checking that the token is a
// TOK_Name matching the given string.
func (t *Token) Match(name string) bool {
	return (t.Str == name && t.Module == "") &&
		(t.Kind == TOK_Name)
}

func (t *Token) IsField() bool {
	return t.Kind == TOK_Name && len(t.Str) >= 2 && t.Str[0] == '.'
}

func (t *Token) IsNamedField() bool {
	if !t.IsField() {
		return false
	}

	runes := []rune(t.Str)
	ch := runes[1]

	return unicode.IsLetter(ch) || ch == '_'
}

// DumpRawTokens is a debugging aid, displays each low-level token.
func (lex *Lexer) DumpRawTokens() {
	for {
		t := lex.rawNext()

		fmt.Printf("line %4d: %s\n", t.LineNum, t.String())

		if t.Kind == TOK_EOF {
			break
		}
	}
}

// DumpTokens is a debugging aid, displays each high-level token.
func (lex *Lexer) DumpTokens() {
	for {
		t := lex.Scan()

		t.DumpFull(0)

		if t.Kind == TOK_EOF {
			break
		}
	}
}

func (t *Token) Dump(level int) {
	fmt.Printf("%*s%s\n", level, "", t.String())

	if t.Children != nil {
		for _, child := range t.Children {
			child.Dump(level + 2)
		}
	}
}

func (t *Token) DumpFull(level int) {
	fmt.Printf("line %4d: %*s%s %s %s\n", t.LineNum, level, "", t.String())

	if t.Children != nil {
		for _, child := range t.Children {
			child.DumpFull(level + 2)
		}
	}
}

func (t *Token) Replace(other *Token) {
	t.Kind = other.Kind
	t.Info = other.Info
	t.Str = other.Str
	t.Module = other.Module
	t.Children = other.Children
}

func (t *Token) ShallowCopy() *Token {
	t2 := new(Token)

	t2.Kind = t.Kind
	t2.Str = t.Str
	t2.LineNum = t.LineNum

	if len(t.Children) > 0 {
		t2.Children = make([]*Token, 0)
		for _, child := range t.Children {
			t2.Add(child)
		}
	}

	return t2
}

func (t *Token) String() string {
	if t == nil {
		return "nil"
	}

	switch t.Kind {
	case TOK_EOF:
		return "EOF"
	case TOK_ERROR:
		return "ERROR '" + t.Str + "'"

	case TOK_Int:
		return "Int '" + t.Str + "'"
	case TOK_Float:
		return "Float '" + t.Str + "'"
	case TOK_String:
		return "String '" + t.Str + "'"
	case TOK_Char:
		return "Char '" + t.Str + "'"

	case TOK_Name:
		if t.Module != "" {
			return "Name '" + t.Module + t.Str + "'"
		}
		return "Name '" + t.Str + "'"
	case TOK_Module:
		return "Module '" + t.Str + "'"

	case TOK_Expr:
		return fmt.Sprintf("Expr (%d elem)", len(t.Children))
	case TOK_Array:
		return fmt.Sprintf("Array (%d elem)", len(t.Children))
	case TOK_Tuple:
		return fmt.Sprintf("Tuple (%d elem)", len(t.Children))
	case TOK_Map:
		return fmt.Sprintf("Map (%d elem)", len(t.Children))
	case TOK_Set:
		return fmt.Sprintf("Set (%d elem)", len(t.Children))

	default:
		return "!!!INVALID!!!"
	}
}

//----------------------------------------------------------------------

func (lex *Lexer) scanLine(s string) {
	// convert line to an array of runes
	runes := []rune(s)

	// check for errors
	for _, ch := range runes {
		if ch == utf8.RuneError {
			lex.addToken(Token{Kind: TOK_ERROR, Str: "bad utf8 in file"})
			return
		}
	}

	lex.scanRunes(runes)
}

func (lex *Lexer) scanRunes(r []rune) {
	for len(r) > 0 {
		// whitespace ?
		if unicode.Is(unicode.White_Space, r[0]) ||
			unicode.IsControl(r[0]) {
			r = r[1:]
			continue
		}

		// comment ?
		if r[0] == ';' {
			break
		}

		// string ?
		if r[0] == '"' {
			size := lex.scanString(r)
			if size < 0 {
				return
			}
			r = r[size:]
			continue
		}

		// character literal ?
		if r[0] == '\'' {
			size := lex.scanCharacter(r)
			if size < 0 {
				return
			}
			r = r[size:]
			continue
		}

		// number ?
		if unicode.IsDigit(r[0]) ||
			(r[0] == '-' && len(r) >= 2 && unicode.IsDigit(r[1])) {

			size := lex.scanNumber(r)
			if size < 0 {
				return
			}
			r = r[size:]
			continue
		}

		// anything else MUST be a name / symbol
		size := lex.scanIdent(r)
		if size <= 0 {
			msg := "illegal character '" + string(r[0]) + "'"
			lex.addToken(Token{Kind: TOK_ERROR, Str: msg})
			return
		}
		r = r[size:]
	}
}

func (lex *Lexer) addToken(t Token) {
	// after an error, ignore any more tokens
	if len(lex.tokens) >= 1 && lex.tokens[0].Kind == TOK_ERROR {
		return
	}

	// errors will "eat" any normal tokens from the line
	if t.Kind == TOK_ERROR {
		lex.tokens = lex.tokens[0:0]
	}

	t.LineNum = lex.line_num

	lex.tokens = append(lex.tokens, &t)
}

func (lex *Lexer) scanString(r []rune) int {
	// skip leading quote
	pos := 1

	s := ""

	for {
		if pos >= len(r) {
			lex.addToken(Token{Kind: TOK_ERROR, Str: "unterminated string"})
			return -1
		}

		if r[pos] == '"' {
			lex.addToken(Token{Kind: TOK_String, Str: s})
			return pos + 1
		}

		// handle escapes
		if r[pos] == '\\' && pos+1 < len(r) {
			pos += 1

			ch, step := lex.scanEscape(r[pos:])
			if step == -2 {
				lex.addToken(Token{Kind: TOK_ERROR, Str: "unknown escape in string"})
				return -1
			} else if step < 0 {
				lex.addToken(Token{Kind: TOK_ERROR, Str: "malformed escape in string"})
				return -1
			}

			s = s + string(ch)
			pos += step
			continue
		}

		s = s + string(r[pos])
		pos += 1
	}
}

func (lex *Lexer) scanCharacter(r []rune) int {
	// skip leading quote
	pos := 1

	if len(r) < 3 || r[1] == '\'' {
		lex.addToken(Token{Kind: TOK_ERROR, Str: "bad character literal"})
		return -1
	}

	if r[2] == '\'' && r[1] != '\\' {
		lex.addToken(Token{Kind: TOK_Char, Str: string(r[1])})
		return 3
	}

	// handle escapes
	if r[pos] != '\\' {
		lex.addToken(Token{Kind: TOK_ERROR, Str: "bad character literal"})
		return -1
	}

	pos += 1
	r = r[pos:]

	ch, step := lex.scanEscape(r)
	if step == -2 {
		lex.addToken(Token{Kind: TOK_ERROR, Str: "unknown escape in char literal"})
		return -1
	} else if step < 0 {
		lex.addToken(Token{Kind: TOK_ERROR, Str: "malformed escape in char literal"})
		return -1
	}

	r = r[step:]

	if len(r) < 1 || r[0] != '\'' {
		lex.addToken(Token{Kind: TOK_ERROR, Str: "unterminated char literal"})
		return -1
	}

	step += pos + 1

	lex.addToken(Token{Kind: TOK_Char, Str: string(ch)})
	return step
}

func (lex *Lexer) scanEscape(r []rune) (rune, int) {
	switch r[0] {
	case '"':
		return '"', 1
	case '\'':
		return '\'', 1
	case '\\':
		return '\\', 1
	case 'a':
		return '\a', 1
	case 'b':
		return '\b', 1
	case 'f':
		return '\f', 1
	case 'n':
		return '\n', 1
	case 'r':
		return '\r', 1
	case 't':
		return '\t', 1
	case 'v':
		return '\v', 1
	}

	// hexadecimal?
	// (requires two hexadecimal digits, no more, no less)
	if r[0] == 'x' {
		return lex.scanHexEscape(r, 2)
	}

	// unicode?
	// (these follow the C11 and Go conventions)
	if r[0] == 'u' {
		return lex.scanHexEscape(r, 4)
	}
	if r[0] == 'U' {
		return lex.scanHexEscape(r, 8)
	}

	// octal?
	// (requires three octal digits, no more, no less)
	if '0' <= r[0] && r[0] <= '3' {
		if len(r) >= 3 &&
			'0' <= r[1] && r[1] <= '7' &&
			'0' <= r[2] && r[2] <= '7' {
			a := r[0] - '0'
			b := r[1] - '0'
			c := r[2] - '0'
			return a*64 + b*8 + c, 3
		}

		return utf8.RuneError, -1
	}

	return utf8.RuneError, -2
}

func (lex *Lexer) scanHexEscape(r []rune, size int) (rune, int) {
	// this assumes first character is part of the escape (the 'x' or 'u')

	if len(r) < size+1 {
		return utf8.RuneError, -1
	}

	var result rune

	for i := 1; i <= size; i++ {
		result = result << 4
		ch := unicode.ToUpper(r[i])
		if '0' <= ch && ch <= '9' {
			result |= (ch - '0')
		} else if 'A' <= ch && ch <= 'F' {
			result |= 10 + (ch - 'A')
		} else {
			return utf8.RuneError, -1
		}
	}

	return result, size + 1
}

// returns # of runes consumed, or negative on error
func (lex *Lexer) scanNumber(r []rune) int {
	pos := 1

	format_hex := false
	format_bin := false

	if (len(r) >= 3 && r[0] == '0' && r[1] == 'x') ||
		(len(r) >= 4 && r[0] == '-' && r[1] == '0' && r[2] == 'x') {
		format_hex = true
		pos = 2
		if r[0] == '-' {
			pos = 3
		}
	}
	if (len(r) >= 3 && r[0] == '0' && r[1] == 'b') ||
		(len(r) >= 4 && r[0] == '-' && r[1] == '0' && r[2] == 'b') {
		format_bin = true
		pos = 2
		if r[0] == '-' {
			pos = 3
		}
	}

	seen_dot := false
	seen_exp := false

	for pos < len(r) {
		// check for an exponent (floating point)
		if !format_hex && !format_bin && (r[pos] == 'e' || r[pos] == 'E') {
			if seen_exp {
				lex.addToken(Token{Kind: TOK_ERROR, Str: "bad float: too many exponents"})
				return -1
			}
			pos += 1
			if pos < len(r) && (r[pos] == '-' || r[pos] == '+') {
				pos += 1
			}
			if !(pos < len(r) && unicode.IsDigit(r[pos])) {
				lex.addToken(Token{Kind: TOK_ERROR, Str: "bad float: no digit after e"})
				return -1
			}
			pos += 1
			seen_exp = true
			continue
		}

		// floating point syntax
		if r[pos] == '.' {
			if format_hex {
				lex.addToken(Token{Kind: TOK_ERROR, Str: "bad hex number"})
				return -1
			}
			if format_bin {
				lex.addToken(Token{Kind: TOK_ERROR, Str: "bad binary number"})
				return -1
			}
			if seen_dot {
				lex.addToken(Token{Kind: TOK_ERROR, Str: "bad float: too many periods"})
				return -1
			}
			pos += 1
			if !(pos < len(r) && unicode.IsDigit(r[pos])) {
				lex.addToken(Token{Kind: TOK_ERROR, Str: "bad float: no digit after period"})
				return -1
			}
			pos += 1
			seen_dot = true
			continue
		}

		// check for end-of-number, and also invalid digits
		if unicode.IsLetter(r[pos]) {
			if format_hex {
				if !unicode.Is(unicode.Hex_Digit, r[pos]) {
					lex.addToken(Token{Kind: TOK_ERROR, Str: "illegal hex digit"})
					return -1
				}
			} else {
				lex.addToken(Token{Kind: TOK_ERROR, Str: "bad number: contains letters"})
				return -1
			}

			pos += 1
			continue

		} else if unicode.IsDigit(r[pos]) {
			if format_bin && !(r[pos] == '0' || r[pos] == '1') {
				lex.addToken(Token{Kind: TOK_ERROR, Str: "illegal binary digit"})
				return -1
			}

			pos += 1
			continue
		}

		break
	}

	kind := TOK_Int
	if seen_dot || seen_exp {
		kind = TOK_Float
	}

	lex.addToken(Token{Kind: kind, Str: string(r[0:pos])})
	return pos
}

// returns # of runes consumed, or negative on error
func (lex *Lexer) scanIdent(r []rune) int {
	pos := 0

	// handle {| and |} symbols for maps
	if (len(r) >= 2 && r[0] == '{' && r[1] == '|') ||
		(len(r) >= 2 && r[0] == '|' && r[1] == '}') {

		lex.addToken(Token{Kind: TOK_Name, Str: string(r[0:2])})
		return 2
	}

	// handle (| and |) symbols for sets
	if (len(r) >= 2 && r[0] == '(' && r[1] == '|') ||
		(len(r) >= 2 && r[0] == '|' && r[1] == ')') {

		lex.addToken(Token{Kind: TOK_Name, Str: string(r[0:2])})
		return 2
	}

	// handle '::' symbol
	if len(r) >= 2 && r[0] == ':' && r[1] == ':' {
		lex.addToken(Token{Kind: TOK_Name, Str: string(r[0:2])})
		return 2
	}

	// handle '...' as a symbol
	if len(r) >= 3 && r[0] == '.' && r[1] == '.' && r[2] == '.' {
		lex.addToken(Token{Kind: TOK_Name, Str: string(r[0:3])})
		return 3
	}

	// handle non-ident symbols
	if strings.ContainsRune("()[]{}:", r[0]) {
		lex.addToken(Token{Kind: TOK_Name, Str: string(r[0:1])})
		return 1
	}

	for pos < len(r) {
		if !LEX_IsIdentChar(r[pos]) {
			break
		}

		if pos+1 < len(r) && r[pos] == '|' && (r[pos+1] == '}' || r[pos+1] == ')') {
			break
		}
		if pos+2 < len(r) && r[pos] == '.' && r[pos+1] == '.' && r[pos+2] == '.' {
			break
		}

		// treat a name like foo/bar as "foo/" followed by "bar".
		if pos > 0 && r[pos] == '/' {
			pos += 1
			break
		}

		pos += 1
	}

	// tell caller we could not parse an identifier
	if pos == 0 {
		return 0
	}

	s := string(r[0:pos])

	if unicode.IsLetter(r[0]) && pos >= 2 && r[pos-1] == '/' {
		lex.addToken(Token{Kind: TOK_Module, Str: s})
	} else {
		lex.addToken(Token{Kind: TOK_Name, Str: s})
	}
	return pos
}

func LEX_IsIdentifier(s string) bool {
	if len(s) == 0 {
		return false
	}

	r := []rune(s)

	// number ?
	if r[0] == '-' || unicode.IsDigit(r[0]) {
		return false
	}

	for _, ch := range r {
		if !LEX_IsIdentChar(ch) {
			return false
		}
	}

	return true
}

func LEX_IsIdentChar(ch rune) bool {
	return false ||
		unicode.IsLetter(ch) ||
		unicode.IsDigit(ch) ||
		strings.ContainsRune(ID_CHARS, ch)
}
